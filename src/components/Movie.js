import React, {Component} from 'react';

class Movie extends Component {
    render() {
        return (
            <div
                className="Movie fadein"
                style={{
                backgroundImage: 'url(' + this.props.poster + ')'
            }}>
                <div className="overlay">
                    <div className="title">{this.props.title}</div>
                    <div className="rating">{this.props.score} / 10</div>
                    <div className="overview">{this.props.overview}</div>
                </div>
            </div>
        );
    }
}

export default Movie;