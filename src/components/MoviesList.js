import React, {Component} from 'react';
import Movie from './Movie';

class MoviesList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      mounted: false,
      api_key: '0f04bbdc102bbd08c7caca24bb575d45'
    };
  }

  fetchMoviesList() {
    var requestUrl = `https://api.themoviedb.org/3/${this.props.url}&api_key=${this.state.api_key}`;
    fetch(requestUrl).then((res) => {
      return res.json();
    }).then((data) => {
      this.setState({data: data});
      console.log(data);
    }).catch((err) => {
      console.log("Error: ", err);
    });
  }

  componentDidMount() {
    if (this.props.url !== '') {
      this.fetchMoviesList();
      this.setState({mounted: true});
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.url !== this.props.url && nextProps.url !== '') {
      this.setState({mounted: true, url: nextProps.url});
      this.fetchMoviesList();
    }
  }

    render() {

      var titles = '';
      if(this.state.data.results) {
        titles = this.state.data.results.map(function(title, index) {
          if(index < 15) {
            var name = '';
            var poster = 'http://image.tmdb.org/t/p/original' + title.backdrop_path;
            if(!title.name) {
              name = title.original_title;
            }
            else {
              name = title.name;
            }
            return (
              <Movie key={title.id} title={name} score={title.vote_average} overview={title.overview} poster={poster} />
            );
          }
        });
      }

      return (
        <div>
          <h1 className="header retroshadow">
            {this.props.title}
          </h1>
          <div ref="titlecategory" className="MoviesList" data-loaded={this.state.mounted}>
            <div className="titles-wrapper">
              {titles}
              </div>
            </div>
        </div>
      );
    }
  }

  export default MoviesList;