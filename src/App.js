import React, {Component} from 'react';
import MoviesList from './components/MoviesList';
import './App.css';

class App extends Component {

  render() {
    return (
      <div className="App">
        <MoviesList
          title="Popular Movies"
          url="discover/movie?sort_by=popularity.desc&page=1"
          className="fadein"/>
      </div>
    );
  }
}

export default App;
